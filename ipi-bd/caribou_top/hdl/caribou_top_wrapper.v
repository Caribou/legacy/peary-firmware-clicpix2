//Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2017.3.1 (lin64) Build 2035080 Fri Oct 20 14:20:00 MDT 2017
//Date        : Tue Jan 23 14:54:52 2018
//Host        : adrian-laptop running 64-bit Ubuntu 17.10
//Command     : generate_target caribou_top_wrapper.bd
//Design      : caribou_top_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module caribou_top_wrapper
   (C3PD_reset_n,
    C3PD_reset_p,
    CLICpix2_miso_n,
    CLICpix2_miso_p,
    CLICpix2_mosi_n,
    CLICpix2_mosi_p,
    CLICpix2_pwr_pulse_n,
    CLICpix2_pwr_pulse_p,
    CLICpix2_reset_n,
    CLICpix2_reset_p,
    CLICpix2_shutter_n,
    CLICpix2_shutter_p,
    CLICpix2_ss_n,
    CLICpix2_ss_p,
    CLICpix2_tp_sw_n,
    CLICpix2_tp_sw_p,
    DDR_addr,
    DDR_ba,
    DDR_cas_n,
    DDR_ck_n,
    DDR_ck_p,
    DDR_cke,
    DDR_cs_n,
    DDR_dm,
    DDR_dq,
    DDR_dqs_n,
    DDR_dqs_p,
    DDR_odt,
    DDR_ras_n,
    DDR_reset_n,
    DDR_we_n,
    FIXED_IO_ddr_vrn,
    FIXED_IO_ddr_vrp,
    FIXED_IO_mio,
    FIXED_IO_ps_clk,
    FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb,
    SI5345_CLK_OUT8_clk_n,
    SI5345_CLK_OUT8_clk_p,
    TLU_BSY_n,
    TLU_BSY_p,
    TLU_RST_n,
    TLU_RST_p,
    TLU_TRG_n,
    TLU_TRG_p,
    Transceiver_RX_n,
    Transceiver_RX_p,
    Transceiver_refClk_clk_n,
    Transceiver_refClk_clk_p,
    led);
  output C3PD_reset_n;
  output C3PD_reset_p;
  input CLICpix2_miso_n;
  input CLICpix2_miso_p;
  output CLICpix2_mosi_n;
  output CLICpix2_mosi_p;
  output CLICpix2_pwr_pulse_n;
  output CLICpix2_pwr_pulse_p;
  output CLICpix2_reset_n;
  output CLICpix2_reset_p;
  output CLICpix2_shutter_n;
  output CLICpix2_shutter_p;
  output CLICpix2_ss_n;
  output CLICpix2_ss_p;
  output CLICpix2_tp_sw_n;
  output CLICpix2_tp_sw_p;
  inout [14:0]DDR_addr;
  inout [2:0]DDR_ba;
  inout DDR_cas_n;
  inout DDR_ck_n;
  inout DDR_ck_p;
  inout DDR_cke;
  inout DDR_cs_n;
  inout [3:0]DDR_dm;
  inout [31:0]DDR_dq;
  inout [3:0]DDR_dqs_n;
  inout [3:0]DDR_dqs_p;
  inout DDR_odt;
  inout DDR_ras_n;
  inout DDR_reset_n;
  inout DDR_we_n;
  inout FIXED_IO_ddr_vrn;
  inout FIXED_IO_ddr_vrp;
  inout [53:0]FIXED_IO_mio;
  inout FIXED_IO_ps_clk;
  inout FIXED_IO_ps_porb;
  inout FIXED_IO_ps_srstb;
  input SI5345_CLK_OUT8_clk_n;
  input SI5345_CLK_OUT8_clk_p;
  output TLU_BSY_n;
  output TLU_BSY_p;
  input TLU_RST_n;
  input TLU_RST_p;
  input TLU_TRG_n;
  input TLU_TRG_p;
  input Transceiver_RX_n;
  input Transceiver_RX_p;
  input Transceiver_refClk_clk_n;
  input Transceiver_refClk_clk_p;
  output [3:0]led;

  wire C3PD_reset_n;
  wire C3PD_reset_p;
  wire CLICpix2_miso_n;
  wire CLICpix2_miso_p;
  wire CLICpix2_mosi_n;
  wire CLICpix2_mosi_p;
  wire CLICpix2_pwr_pulse_n;
  wire CLICpix2_pwr_pulse_p;
  wire CLICpix2_reset_n;
  wire CLICpix2_reset_p;
  wire CLICpix2_shutter_n;
  wire CLICpix2_shutter_p;
  wire CLICpix2_ss_n;
  wire CLICpix2_ss_p;
  wire CLICpix2_tp_sw_n;
  wire CLICpix2_tp_sw_p;
  wire [14:0]DDR_addr;
  wire [2:0]DDR_ba;
  wire DDR_cas_n;
  wire DDR_ck_n;
  wire DDR_ck_p;
  wire DDR_cke;
  wire DDR_cs_n;
  wire [3:0]DDR_dm;
  wire [31:0]DDR_dq;
  wire [3:0]DDR_dqs_n;
  wire [3:0]DDR_dqs_p;
  wire DDR_odt;
  wire DDR_ras_n;
  wire DDR_reset_n;
  wire DDR_we_n;
  wire FIXED_IO_ddr_vrn;
  wire FIXED_IO_ddr_vrp;
  wire [53:0]FIXED_IO_mio;
  wire FIXED_IO_ps_clk;
  wire FIXED_IO_ps_porb;
  wire FIXED_IO_ps_srstb;
  wire SI5345_CLK_OUT8_clk_n;
  wire SI5345_CLK_OUT8_clk_p;
  wire TLU_BSY_n;
  wire TLU_BSY_p;
  wire TLU_RST_n;
  wire TLU_RST_p;
  wire TLU_TRG_n;
  wire TLU_TRG_p;
  wire Transceiver_RX_n;
  wire Transceiver_RX_p;
  wire Transceiver_refClk_clk_n;
  wire Transceiver_refClk_clk_p;
  wire [3:0]led;

  caribou_top caribou_top_i
       (.C3PD_reset_n(C3PD_reset_n),
        .C3PD_reset_p(C3PD_reset_p),
        .CLICpix2_miso_n(CLICpix2_miso_n),
        .CLICpix2_miso_p(CLICpix2_miso_p),
        .CLICpix2_mosi_n(CLICpix2_mosi_n),
        .CLICpix2_mosi_p(CLICpix2_mosi_p),
        .CLICpix2_pwr_pulse_n(CLICpix2_pwr_pulse_n),
        .CLICpix2_pwr_pulse_p(CLICpix2_pwr_pulse_p),
        .CLICpix2_reset_n(CLICpix2_reset_n),
        .CLICpix2_reset_p(CLICpix2_reset_p),
        .CLICpix2_shutter_n(CLICpix2_shutter_n),
        .CLICpix2_shutter_p(CLICpix2_shutter_p),
        .CLICpix2_ss_n(CLICpix2_ss_n),
        .CLICpix2_ss_p(CLICpix2_ss_p),
        .CLICpix2_tp_sw_n(CLICpix2_tp_sw_n),
        .CLICpix2_tp_sw_p(CLICpix2_tp_sw_p),
        .DDR_addr(DDR_addr),
        .DDR_ba(DDR_ba),
        .DDR_cas_n(DDR_cas_n),
        .DDR_ck_n(DDR_ck_n),
        .DDR_ck_p(DDR_ck_p),
        .DDR_cke(DDR_cke),
        .DDR_cs_n(DDR_cs_n),
        .DDR_dm(DDR_dm),
        .DDR_dq(DDR_dq),
        .DDR_dqs_n(DDR_dqs_n),
        .DDR_dqs_p(DDR_dqs_p),
        .DDR_odt(DDR_odt),
        .DDR_ras_n(DDR_ras_n),
        .DDR_reset_n(DDR_reset_n),
        .DDR_we_n(DDR_we_n),
        .FIXED_IO_ddr_vrn(FIXED_IO_ddr_vrn),
        .FIXED_IO_ddr_vrp(FIXED_IO_ddr_vrp),
        .FIXED_IO_mio(FIXED_IO_mio),
        .FIXED_IO_ps_clk(FIXED_IO_ps_clk),
        .FIXED_IO_ps_porb(FIXED_IO_ps_porb),
        .FIXED_IO_ps_srstb(FIXED_IO_ps_srstb),
        .SI5345_CLK_OUT8_clk_n(SI5345_CLK_OUT8_clk_n),
        .SI5345_CLK_OUT8_clk_p(SI5345_CLK_OUT8_clk_p),
        .TLU_BSY_n(TLU_BSY_n),
        .TLU_BSY_p(TLU_BSY_p),
        .TLU_RST_n(TLU_RST_n),
        .TLU_RST_p(TLU_RST_p),
        .TLU_TRG_n(TLU_TRG_n),
        .TLU_TRG_p(TLU_TRG_p),
        .Transceiver_RX_n(Transceiver_RX_n),
        .Transceiver_RX_p(Transceiver_RX_p),
        .Transceiver_refClk_clk_n(Transceiver_refClk_clk_n),
        .Transceiver_refClk_clk_p(Transceiver_refClk_clk_p),
        .led(led));
endmodule
