`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.08.2017 11:37:20
// Design Name: 
// Module Name: ClockBuffer
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ClockBuffer(
    input Clk_in_p,
    input Clk_in_n,
    output Clk_out
    );
    
    IBUFGDS ibufgds_inst (
    .O (Clk_out),
    .I (Clk_in_p),
    .IB (Clk_in_n) );
endmodule
