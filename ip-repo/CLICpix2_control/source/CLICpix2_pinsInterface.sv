//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_pinsInterface.sv
// Description     : The interface combined CLICpix2 pin signals
// Author          : Adrian Fiergolski
// Created On      : Mon May  8 16:34:58 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.


interface CLICpix2_pinsInterface;
   logic tp_sw;
   logic shutter;
   logic pwr_pulse;
   logic reset;
endinterface // CLICpix2_pinsInterface
   
   
   
